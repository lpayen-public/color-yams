const $ = require("jquery");
let xhrMain = new XMLHttpRequest();

export function openLoader () {
    if ($('.loader').length) {
        return false;
    }
    closeLoader();
    let $loader = '<div class="loader">';
    $loader += '<div class="loader-content">';
    $loader += '<i class="fa fa-circle-notch fa-spin"></i>';
    $loader += '</div></div>';
    $('main').append($loader);
    setTimeout(closeLoader, 5000);
}

export function closeLoader () {
    $('.loader').remove();
    return false;
}

$(document).on('click', '.js-openLoader', function () {
    openLoader();
});

$(document).on('click', '#js-btn-delete-all', function () {

    if (!confirm("Etes-vous sûr de vouloir supprimer toutes les parties ?")) {
        return false;
    }

    openLoader();
    location.href = $(this).attr('href');
});

export function getRandomInt2 (max) {
    return Math.floor(Math.random() * max);
}

export function inArray (needle, haystack) {
    let length = haystack.length;
    for (let i = 0; i < length; i++) {
        if (haystack[i] == needle)
            return true;
    }
    return false;
}
