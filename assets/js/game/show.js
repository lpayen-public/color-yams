import {openLoader} from "../main";
const $ = require("jquery");
let xhrGameShow = new XMLHttpRequest();
$(document).on('click', '.js-delete-score', function () {

    let $this = $(this);
    let token = $this.attr('data-token');

    if (!token) {
        return false;
    }

    if (!confirm("Etes-vous sûr de vouloir supprimer ce score ?")) {
        return;
    }

    openLoader();
    xhrGameShow.abort();

    xhrGameShow = $.ajax({
        url: Routing.generate('game_delete_score_ajax'),
        method: "post",
        data: {token: token}
    }).done(function () {
        location.reload();
    });

    return false;
});