const $ = require("jquery");
// Sélection/déselection d'un user
$(document).on('click', '.js-user', function () {

    let $this = $(this);
    let token = $this.attr('data-token');

    if ($this.hasClass('is-in')) {
        $('#tokenDelete').val(token);
    } else {
        $('#tokenAdd').val(token);
    }

    $('#form-add').submit();
});

// Création de la partie
$(document).on('click', '#js-play', function () {
    $('#isPlay').val(1);
    $('#form-add').submit();
});