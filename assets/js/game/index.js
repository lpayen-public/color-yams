import {openLoader} from "../main";
const $ = require("jquery");
let xhrGameIndex = new XMLHttpRequest();

// Suppression d'une partie
$(document).on('click', '.js-game-delete', function (e) {

    e.preventDefault();
    e.stopPropagation();

    if (!confirm("Etes-vous sûr de vouloir supprimer cette partie ?")) {
        return;
    }

    let $this = $(this);
    let token = $this.attr('data-token');

    openLoader();
    xhrGameIndex.abort();

    xhrGameIndex = $.ajax({
        url: Routing.generate('game_delete_ajax'),
        method: "post",
        data: {token: token}
    }).done(function (response) {
        if (response === true) {
            $('#js-game' + token).remove();
        }
        closeLoader();
    });
});