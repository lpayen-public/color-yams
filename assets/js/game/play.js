import {getRandomInt2, inArray, openLoader} from "../main";

const $ = require("jquery");
let $score = $('#score');
let $scoreHtml = $('#score-html');
let $form = $('#form-stroke');
let $dices = $('#dices');
let values = [];
let id;
let num;
let color;
let score;
let numRoll = 0;
let today = $('#js-today').val();

$(document).on('submit', '#form-stroke', function () {
    openLoader();
});

function showScore () {

    let score = $score.val();
    $scoreHtml.removeClass('color-b fs16 fwn');

    if (score === '') {
        $scoreHtml.addClass('color-b fs16 fwn')
        score = $scoreHtml.attr('data-placeholder');
    }

    $scoreHtml.html(score);
}

function getScore () {
    return $score.val();
}

export function setScore (score) {
    $score.val(score);
    return showScore();
}

function deleteScore () {
    let score = getScore();
    score = score.substring(0, score.length - 1);
    setScore(score);
    setScore('');
}

$(document).on('click', '.js-touch', function () {

    let $this = $(this);
    let key = $this.attr('data-key');
    let score = getScore();

    if (key === 'delete') {
        return deleteScore();
    }

    if (key === 'save') {

        if (!checkSelectedDices()) {
            return false;
        }

        if (parseInt(score) > 5 || parseInt(score) == 0) {
            $form.submit();
        }
        return;
    }

    // let scoreNew = eval(score + key);
    let scoreNew = parseInt(score + key);

    if (scoreNew > 36 || scoreNew === 4 || scoreNew === 5) {
        return setScore(score);
    }

    setScore(scoreNew);
});

showScore();

function countPoints () {
    score = 0;
    $('.dice.selected').each(function () {
        score += parseInt($(this).attr('data-num'));
    });
    setScore(score);
}

function calculateDices () {
    numRoll++;
    $('#rollNum').html(numRoll + '/3');
    countPoints();
    if (numRoll >= 3) {
        $('#roll').addClass('none');
        $('#zeroScore').removeClass('none');
        $('#touches').removeClass('none');
        $('#afterRolls').removeClass('none');
    }
}

function roll () {
    id = 0;
    while (!id || inArray(id, values)) {
        num = (getRandomInt2(6) + 1).toString();
        color = (getRandomInt2(6) + 1).toString();
        id = num + color;
    }
    values.push(id);
}

function checkSelectedDices () {

    let isOk = $('.dice').not('.selected').length == 0;
    $('.js-save-player').css({opacity: 1});
    if (!isOk) {
        $('.js-save-player').css({opacity: .35});
        $('#roll').removeClass('disabled');
    }
    else {
        $('#roll').addClass('disabled');
    }

    return isOk;
}

$.fn.dice = function () {
    let $diceRow = $('<div>').addClass('row');
    for (let i = 1; i <= 6; i++) {
        roll();
        let img = '<img src="/img/dice/' + id + '.png?' + today + '" class="w100p">';
        let $col = $('<div>').addClass('col-4 col-sm-2 col-xl-2 text-center');
        let $box = $('<div>').addClass('box pushed-touch');
        let $dice = $('<div>')
            .addClass('box-body p0 text-center dice selected')
            .attr('id', 'dice' + id)
            .attr('data-id', id)
            .attr('data-num', num)
            .html(img)
            .on('click', function () {
                $(this).toggleClass('selected');
                checkSelectedDices();
                countPoints();
                return false;
            });
        $diceRow.append($col);
        $col.append($box);
        $box.append($dice);
    }
    $dices.prepend($diceRow);
    calculateDices();
};

$(document).on('click', '#roll', function () {
    if ($('#roll').hasClass('disabled')) {
        return false;
    }
    values = [];
    // On prend d'abord ceux qui restent pour être sûr qu'ils ne soient pas en doublon ensuite
    $('.dice').each(function () {
        let $this = $(this);
        if ($this.hasClass('selected')) {
            values.push($this.attr('data-id'));
        }
    });
    // Une fois ceux qui restent enregistrés, on peut lancer le random qui va checker ce qu'il ne faut pas prendre
    $('.dice').each(function () {
        let $this = $(this);
        if (!$this.hasClass('selected')) {
            roll();
            let img = '<img src="/img/dice/' + id + '.png" class="w100p">';
            $this
                .addClass('selected')
                .attr('data-num', num)
                .attr('data-id', id)
                .html(img);
        }
    });
    checkSelectedDices();
    calculateDices();
});

$(document).on('click', '#calculator', function () {
    let $this = $(this);
    if (!$this.hasClass('selected')) {
        $('#numbers').removeClass('none');
    } else {
        $('#numbers').addClass('none');
    }
    $this.toggleClass('selected');
});

$(document).on('click', '#zeroScore', function () {
    setScore(0);
});

if($dices.length){
$dices.dice();
}