import './routing.js';
import * as bootstrap from 'bootstrap';
const $ = require('jquery');
global.$ = global.jQuery = $;
require('jqueryui');


import './js/main';
import './js/game/add';
import './js/game/index';
import './js/game/play';
import './js/game/show';

import './styles/app.css';
