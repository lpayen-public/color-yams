<?php

namespace App\Twig;

use App\Helpers\Date;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    /**
     * @return TwigFilter[]
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('dateHour', [$this, 'getDateHour']),
            new TwigFilter('score', [$this, 'getScore']),
            new TwigFilter('color', [$this, 'getColor']),
        ];
    }

    public function getDateHour(\DateTime $date): string
    {
        return Date::getDateHour($date);
    }

    public function getScore(int $nb): string
    {
        return $nb . ' point' . ($nb > 1 ? 's' : '');
    }

    public function getColor(string $color, float $alpha = 1): string
    {
        return 'background:rgba(' . $color . ',' . $alpha . ');';
    }
}
