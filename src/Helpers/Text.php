<?php

namespace App\Helpers;

class Text
{
    /**
     * Créé une chaine aléatoire.
     */
    public static function random(int $lenth = 8, bool $isSpecialChar = false, bool $isUpper = false): string
    {
        $tab0 = [];
        for ($i = 0; $i < 10; ++$i) {
            $tab0[] = $i;
        }

        $tab1 = [];
        for ($i = 97; $i <= 122; ++$i) {
            $tab1[] = \chr($i);
        }

        $tab2 = ['!', '$', '#'];

        $tab3 = [];
        for ($i = 65; $i <= 90; ++$i) {
            $tab3[] = \chr($i);
        }

        $tabAll = [$tab0, $tab1];

        if ($isSpecialChar) {
            $tabAll[] = $tab2;
        }

        if ($isUpper) {
            $tabAll[] = $tab3;
        }

        $nbTab = \count($tabAll);
        $tabP = [];

        while (\count($tabP) < $lenth) {
            $rand = \rand(0, $nbTab - 1);
            $tab = $tabAll[$rand];
            $rand = \rand(0, \count($tab) - 1);
            $val = $tab[$rand];
            $tabP[] = $val;
        }

        return \implode('', $tabP);
    }

    public function getPlural(int $nb, string $letter = 's'): string
    {
        return $nb > 1 ? $letter : '';
    }
}
