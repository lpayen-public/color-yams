<?php

namespace App\Helpers;

use Symfony\Component\HttpFoundation\RequestStack;

class Nav
{
    private RequestStack $request;

    private string $className = 'active';

    public function __construct(RequestStack $rq)
    {
        $this->request = $rq;
    }

    /**
     * Liste des menus.
     *
     * @return string[][]
     */
    public function getMenus(): array
    {
        return [
            ['icon' => 'home', 'link' => 'index', 'route' => null, 'strictRoute' => 'index'],
            ['icon' => 'gamepad', 'link' => 'game_index', 'route' => 'game_', 'strictRoute' => null],
            ['icon' => 'user', 'link' => 'user_index', 'route' => 'user_', 'strictRoute' => null],
            ['icon' => 'terminal', 'link' => 'option_index', 'route' => 'logs_', 'strictRoute' => null],
        ];
    }

    /**
     * Retourne si le menu est actif ou non.
     */
    public function isActive(?string $route, string $strictRoute = null): ?string
    {
        if ($route) {
            return false !== strpos($this->getRoute(), $route) ? $this->className : null;
        }

        return $this->getRoute() === $strictRoute ? $this->className : null;
    }

    /**
     * Retourne la route.
     */
    public function getRoute(): string
    {
        return $this->request->getCurrentRequest()->get('_route');
    }
}
