<?php

namespace App\Helpers;

class Date
{
    public static function getDateHour(\DateTime $date): string
    {
        return $date->format('d/m/Y H\hi');
    }
}
