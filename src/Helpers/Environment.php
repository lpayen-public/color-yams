<?php

namespace App\Helpers;

class Environment
{
    public function isProd(): bool
    {
        return (bool) \preg_match('#(eosia)#', __DIR__);
    }

    public function isLocal(): bool
    {
        return !$this->isProd();
    }

    public function getEnvironment(): string
    {
        return $this->isProd() ? 'prod' : 'local';
    }

    public function getCache(): string
    {
        return $this->isProd() ? '?' . \date('YmdHi') : '?' . \date('YmdHis');
    }
}
