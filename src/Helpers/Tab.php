<?php

namespace App\Helpers;

use App\Service\UserService;

class Tab
{
    /**
     * Retourne un tableau dont la clé de chaque élément est sa $key.
     *
     * @see UserService::getUsersPlayers()
     */
    public static function reorderBy(array $elements, string $key = 'id'): array
    {
        $tab = [];
        $getter = 'get' . \ucwords($key);

        foreach ($elements as $e) {
            $tab[$e->{$getter}()] = $e;
        }

        return $tab;
    }
}
