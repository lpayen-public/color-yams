<?php

namespace App\Helpers;

class Html
{
    private bool $isBtnBlock = false;

    private string $width = 'lg';

    private function block(): string
    {
        return $this->isBtnBlock ? ' btn-block' : ' ';
    }

    public function table(): string
    {
        return 'table';
    }

    public function datatable(): string
    {
        return 'datatable';
    }

    private function btn(string $color, string $width = null): string
    {
        return 'btn btn-' . $this->getBtnWidth($width) . ' btn-' . $color . $this->block() . $this->btnDefault();
    }

    private function getBtnWidth(?string $width): string
    {
        return $width ?: $this->width;
    }

    private function btnDefault(): string
    {
        return ' fs12 text-uppercase';
    }

    public function see(string $width = null): string
    {
        return $this->btn('primary', $width);
    }

    public function update(string $width = null): string
    {
        return $this->btn('warning', $width);
    }

    public function delete(string $width = null): string
    {
        return $this->btn('danger', $width);
    }

    public function save(string $width = null): string
    {
        return $this->btn('success', $width);
    }

    public function validate(string $width = null): string
    {
        return $this->btn('success', $width);
    }
}
