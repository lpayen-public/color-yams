<?php

namespace App;

use App\Entity\Challenge;
use App\Entity\Game;
use App\Entity\Goal;
use App\Entity\Player;
use App\Entity\Step;
use App\Entity\Stroke;
use App\Entity\User;
use App\Helpers\Text;
use App\Service\GameService;
use App\Service\GridService;
use App\Service\PlayService;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface as EMI;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class YamsController extends AbstractController
{
    protected EMI $em;

    protected GameService $gameService;

    protected GridService $gridService;

    protected UserService $userService;

    protected PlayService $playService;

    public function __construct(EMI $em, GameService $gas, GridService $grs, UserService $us, PlayService $ps)
    {
        $this->em = $em;
        $this->gameService = $gas;
        $this->gridService = $grs;
        $this->userService = $us;
        $this->playService = $ps;

        //        $this->setMissedToken();
    }

    /**
     * Settage des tokens s'ils n'existent pas.
     *
     * @return void
     */
    protected function setMissedToken()
    {
        /** @var array $entities */
        $entities = [
            Game::class,
            Player::class,
            User::class,
            Step::class,
            Challenge::class,
            Goal::class,
            Stroke::class,
        ];

        foreach ($entities as $entity) {
            $entityRepos = $this->em->getRepository($entity)->findAll();

            foreach ($entityRepos as $entityRepo) {
                $entityRepo->setToken(Text::random());
            }
        }

        $this->em->flush();
    }

    /**
     * Retourne le dossier racine du logiciel.
     */
    protected function getRoot(): string
    {
        return $this->getParameter('kernel.project_dir');
    }

    /**
     * Retourne le dossier racine des logs.
     */
    protected function getRootLogs(): string
    {
        return $this->getRoot() . '/var/log/';
    }
}
