<?php

namespace App\Entity;

use App\Repository\GameRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GameRepository::class)
 */
class Game
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?\DateTimeInterface $date = null;

    /**
     * @ORM\OneToMany(targetEntity=Player::class, mappedBy="game", orphanRemoval=true, cascade={"persist", "remove"})
     *
     * @ORM\OrderBy({"order"="ASC", "id"="ASC"})
     */
    private Collection $players;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $token;

    /**
     * @ORM\OneToMany(targetEntity=Goal::class, mappedBy="game", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private Collection $goals;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": 0})
     */
    private bool $isOver = false;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": 0})
     */
    private bool $isVirtualDices = false;

    public function __construct()
    {
        if (!$this->getDate()) {
            $this->setDate(new \DateTime());
        }
        $this->players = new ArrayCollection();
        $this->goals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @return $this
     */
    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection<int, Player>
     */
    public function getPlayers(): Collection
    {
        return $this->players;
    }

    /**
     * @return $this
     */
    public function addPlayer(Player $player): self
    {
        if (!$this->players->contains($player)) {
            $this->players[] = $player;
            $player->setGame($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removePlayer(Player $player): self
    {
        if ($this->players->removeElement($player)) {
            // set the owning side to null (unless already changed)
            if ($player->getGame() === $this) {
                $player->setGame(null);
            }
        }

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @return $this
     */
    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return Collection<int, Goal>
     */
    public function getGoals(): Collection
    {
        return $this->goals;
    }

    /**
     * @return $this
     */
    public function addGoal(Goal $goal): self
    {
        if (!$this->goals->contains($goal)) {
            $this->goals[] = $goal;
            $goal->setGame($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeGoal(Goal $goal): self
    {
        if ($this->goals->removeElement($goal)) {
            // set the owning side to null (unless already changed)
            if ($goal->getGame() === $this) {
                $goal->setGame(null);
            }
        }

        return $this;
    }

    public function getIsOver(): ?bool
    {
        return $this->isOver;
    }

    /**
     * @return $this
     */
    public function setIsOver(bool $isOver): self
    {
        $this->isOver = $isOver;

        return $this;
    }

    public function isIsOver(): ?bool
    {
        return $this->isOver;
    }

    public function getIsVirtualDices(): ?bool
    {
        return $this->isVirtualDices;
    }

    /**
     * @return $this
     */
    public function setIsVirtualDices(bool $isVirtualDices): self
    {
        $this->isVirtualDices = $isVirtualDices;

        return $this;
    }

    public function isIsVirtualDices(): ?bool
    {
        return $this->isVirtualDices;
    }
}
