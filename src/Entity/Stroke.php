<?php

namespace App\Entity;

use App\Repository\StrokeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StrokeRepository::class)
 */
class Stroke
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Player::class, inversedBy="strokes")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Player $player;

    /**
     * @ORM\ManyToOne(targetEntity=Goal::class)
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Goal $goal;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $score;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $bonus;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $token;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    /**
     * @return $this
     */
    public function setPlayer(?Player $player): self
    {
        $this->player = $player;

        return $this;
    }

    public function getGoal(): ?Goal
    {
        return $this->goal;
    }

    /**
     * @return $this
     */
    public function setGoal(?Goal $goal): self
    {
        $this->goal = $goal;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    /**
     * @return $this
     */
    public function setScore(?int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getBonus(): ?int
    {
        return $this->bonus;
    }

    /**
     * @return $this
     */
    public function setBonus(?int $bonus): self
    {
        $this->bonus = $bonus;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @return $this
     */
    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }
}
