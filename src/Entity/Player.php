<?php

namespace App\Entity;

use App\Repository\PlayerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlayerRepository::class)
 */
class Player
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class, inversedBy="players")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Game $game;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="players")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $user;

    /**
     * @ORM\Column(name="`order`", type="integer", nullable=false, options={"default": 0})
     */
    private int $order = 0;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $token;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default": 0})
     */
    private int $score = 0;

    /**
     * @ORM\OneToMany(targetEntity=Stroke::class, mappedBy="player", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private Collection $strokes;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $position;

    public function __construct()
    {
        $this->strokes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    /**
     * @return $this
     */
    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @return $this
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getOrder(): ?int
    {
        return $this->order;
    }

    /**
     * @return $this
     */
    public function setOrder(int $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @return $this
     */
    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getScore(): int
    {
        return $this->score;
    }

    /**
     * @return $this
     */
    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    /**
     * @return Collection<int, Stroke>
     */
    public function getStrokes(): Collection
    {
        return $this->strokes;
    }

    /**
     * @return $this
     */
    public function addStroke(Stroke $stroke): self
    {
        if (!$this->strokes->contains($stroke)) {
            $this->strokes[] = $stroke;
            $stroke->setPlayer($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeStroke(Stroke $stroke): self
    {
        if ($this->strokes->removeElement($stroke)) {
            // set the owning side to null (unless already changed)
            if ($stroke->getPlayer() === $this) {
                $stroke->setPlayer(null);
            }
        }

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @return $this
     */
    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }
}
