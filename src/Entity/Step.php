<?php

namespace App\Entity;

use App\Repository\StepRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StepRepository::class)
 */
class Step
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $token;

    /**
     * @ORM\OneToMany(targetEntity=Challenge::class, mappedBy="step", orphanRemoval=true)
     */
    private Collection $challenges;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $color;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default": 2})
     */
    private int $nbChallenge = 2;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default": 1})
     */
    private int $nbColorYams = 1;

    /**
     * @ORM\Column(name="`order`", type="integer", nullable=false, options={"default": 0})
     */
    private int $order = 0;

    public function __construct()
    {
        $this->challenges = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @return $this
     */
    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return Collection<int, Challenge>
     */
    public function getChallenges(): Collection
    {
        return $this->challenges;
    }

    /**
     * @return $this
     */
    public function addChallenge(Challenge $challenge): self
    {
        if (!$this->challenges->contains($challenge)) {
            $this->challenges[] = $challenge;
            $challenge->setStep($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeChallenge(Challenge $challenge): self
    {
        if ($this->challenges->removeElement($challenge)) {
            // set the owning side to null (unless already changed)
            if ($challenge->getStep() === $this) {
                $challenge->setStep(null);
            }
        }

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @return $this
     */
    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getNbChallenge(): ?int
    {
        return $this->nbChallenge;
    }

    /**
     * @return $this
     */
    public function setNbChallenge(int $nbChallenge): self
    {
        $this->nbChallenge = $nbChallenge;

        return $this;
    }

    public function getNbColorYams(): ?int
    {
        return $this->nbColorYams;
    }

    /**
     * @return $this
     */
    public function setNbColorYams(int $nbColorYams): self
    {
        $this->nbColorYams = $nbColorYams;

        return $this;
    }

    public function getOrder(): ?int
    {
        return $this->order;
    }

    public function setOrder(int $order): self
    {
        $this->order = $order;

        return $this;
    }
}
