<?php

namespace App\Entity;

use App\Repository\ChallengeTestRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ChallengeTestRepository::class)
 */
class ChallengeTest
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $token;

    /**
     * @ORM\ManyToOne(targetEntity=Step::class, inversedBy="challenges")
     */
    private ?Step $step;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @return $this
     */
    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getStep(): ?Step
    {
        return $this->step;
    }

    /**
     * @return $this
     */
    public function setStep(?Step $step): self
    {
        $this->step = $step;

        return $this;
    }
}
