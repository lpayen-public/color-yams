<?php

namespace App\Command;

use App\Entity\Challenge;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UpdateChallengeCommand extends Command
{
    protected static $defaultName = 'ct:update:challenge';
    protected static $defaultDescription = 'Add a short description for your command';

    public function __construct(private readonly EntityManagerInterface $em, string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $tab = [
            [
                'search' => '"ml10 mr10"',
                'replace' => '"ml10 mr10 sign-up"',
            ],
            [
                'search' => '"mr5"',
                'replace' => '"mr5 sign-up"',
            ],
            [
                'search' => '"ml5 mr5"',
                'replace' => '"ml10 mr10 sign-up"',
            ],
            [
                'search' => '1 x <img',
                'replace' => '<span class="mr5 sign-up">1 x</span><img',
            ],
            [
                'search' => '<span class="mr5 sign-up">1 x <img',
                'replace' => '<span class="mr10 sign-up">1 x <img',
            ],
            [
                'search' => 'ml10 mr10',
                'replace' => 'ml15 mr15',
            ],
            [
                'search' => '"mr5',
                'replace' => '"mr10',
            ],
            [
                'search' => 'ml15 mr15',
                'replace' => 'ml10 mr10',
            ],
            [
                'search' => 'ml10 mr10 sign-up fs17',
                'replace' => 'ml10 mr10 sign-up',
            ],
        ];

        $challenges = $this->em->getRepository(Challenge::class)->findAll();

        /** @var Challenge $challenge */
        foreach ($challenges as $challenge) {
            $old = $challenge->getName();
            $name = $old;

            foreach ($tab as $d) {
                $name = \str_replace($d['search'], $d['replace'], $name);
            }

            if ($old !== $name) {
                $io->info($old . PHP_EOL . $name);
            }

            $challenge->setName($name);
        }

        $this->em->flush();

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return Command::SUCCESS;
    }
}
