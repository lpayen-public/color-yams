<?php

namespace App\Controller;

use App\Entity\Game;
use App\YamsController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GameController extends YamsController
{
    /**
     * Liste des parties.
     */
    public function index(): Response
    {
        return $this->render('game/index.html.twig', [
            'games' => $this->gameService->getGames(),
        ]);
    }

    /**
     * Affichage d'une partie, de ses players, de sa grille de scores.
     */
    public function show(Game $game): Response
    {
        return $this->render('game/show.html.twig', [
            'game' => $game,
            'grid' => $this->gridService->getGrid($game),
            'isReloadable' => $this->gameService->isReloadPossible($game),
            'isRevengePossible' => $this->gameService->isRevengePossible($game),
        ]);
    }

    public function play(Game $game, Request $request): RedirectResponse|Response
    {
        $datas = $this->playService->play($game, $request);
        $stroke = $datas['stroke'];
        $score = $datas['score'];

        // Partie finie
        if (null === $stroke) {
            return $this->redirectToRoute('game_show', [
                'token' => $game->getToken(),
            ]);
        }

        // Saisie d'un score
        if (null !== $score) {
            return $this->redirectToRoute('game_play', [
                'token' => $game->getToken(),
            ]);
        }

        return $this->render('game/play.html.twig', $datas);
    }

    /**
     * Création d'une partie après le choix des users.
     */
    public function add(Request $request): Response
    {
        $isPlay = $request->get('isPlay');
        [$usersOut, $usersIn, $isVirtualDices] = $this->userService->getUsersPlayers($request);

        // Ajout des users
        if (!$isPlay) {
            return $this->render('game/add.html.twig', [
                'usersOut' => $usersOut,
                'usersIn' => $usersIn,
                'isVirtualDices' => $isVirtualDices,
            ]);
        }

        // Création de la partie
        $game = $this->gameService->addGame(null, $usersIn, $isVirtualDices);

        return $this->redirectToRoute('game_show', [
            'token' => $game->getToken(),
        ]);
    }

    /**
     * Suppression d'une game.
     */
    public function delete(Request $request): JsonResponse
    {
        $token = $request->get('token');
        $return = $this->gameService->delete($token);

        return new JsonResponse($return);
    }

    public function deleteScore(Request $request): JsonResponse
    {
        $token = $request->get('token');
        $return = $this->gameService->deleteScore($token);

        return new JsonResponse($return);
    }

    public function reload(Game $game): RedirectResponse
    {
        $this->gameService->reload($game);

        return $this->redirectToRoute('game_show', ['token' => $game->getToken()]);
    }

    public function revenge(Game $oldGame): RedirectResponse
    {
        $game = $this->gameService->revenge($oldGame);

        return $this->redirectToRoute('game_show', [
            'token' => $game->getToken(),
        ]);
    }
}
