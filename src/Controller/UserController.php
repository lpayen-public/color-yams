<?php

namespace App\Controller;

use App\YamsController;
use Symfony\Component\HttpFoundation\Response;

class UserController extends YamsController
{
    /**
     * Liste des users.
     */
    public function index(): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $this->userService->getUsers(),
        ]);
    }
}
