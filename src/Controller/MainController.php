<?php

namespace App\Controller;

use App\Service\ChallengeService;
use App\YamsController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class MainController extends YamsController
{
    public function index(): Response
    {
        return $this->render('main/index.html.twig', [
            'averages' => $this->userService->getUsersByRanking(),
            'scores' => $this->userService->getUsersByBestScore(),
        ]);
    }

    public function deleteGames(ChallengeService $challengeService): RedirectResponse
    {
        //        $challengeService->init(true);

        return $this->redirectToRoute('option_index');
    }

    public function challenge(ChallengeService $challengeService): Response
    {
        [$challenges1, $challenges2, $challenges3] = $challengeService->init();

        return $this->render('main/challenge.html.twig', [
            'challenges1' => $challenges1,
            'challenges2' => $challenges2,
            'challenges3' => $challenges3,
        ]);
    }

    public function option(): Response
    {
        $files = \glob($this->getRootLogs() . '*');
        $logs = [];
        foreach ($files as $file) {
            $lines = \file($file);

            foreach ($lines as $line) {
                if (!\preg_match('#(ERROR|CRITICAL)#', $line)) {
                    continue;
                }

                $lineDate = \substr($line, 0, 34);
                $date = \explode('-', \substr($lineDate, 1, 10));
                $hour = \substr($lineDate, 12, 8);
                $dateFr = $date[2] . '/' . $date[1] . '/' . $date[0] . ' ' . $hour;
                $line = \str_replace($lineDate, '', $line);
                $line = \preg_replace('#(at )#', 'at <br>', $line);

                $line = \str_replace(['request.ERROR: ', 'request.CRITICAL: '], ['', ''], $line);
                $line = \str_replace('\\\\', '\\', $line);
                $logs[] = ['date' => $dateFr, 'text' => $line];
            }
        }

        return $this->render('main/option.html.twig', [
            'logs' => $logs,
        ]);
    }

    public function deleteLogs(): RedirectResponse
    {
        foreach (\glob($this->getRootLogs() . '*') as $file) {
            \unlink($file);
        }

        return $this->redirectToRoute('option_index');
    }
}
