<?php

namespace App\Service;

use App\Entity\Challenge;
use App\Entity\Game;
use App\Entity\Goal;
use App\Entity\Player;
use App\Entity\Step;
use App\Entity\Stroke;
use App\Helpers\Text;
use App\Repository\StrokeRepository;
use Doctrine\ORM\EntityManagerInterface;

class GameService
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly StepService $stepService,
        private readonly UserService $userService,
        private readonly PlayService $playService
    ) {
    }

    public function getGames(): array
    {
        $gs = $this->em->getRepository(Game::class)->findBy([], ['id' => 'DESC']);

        $games = [];

        /** @var Game $g */
        foreach ($gs as $g) {
            $games[$g->getId()] = [
                'data' => $g,
                'players' => [],
            ];

            /** @var Player $p */
            foreach ($g->getPlayers() as $p) {
                $games[$g->getId()]['players'][$p->getId()] = [
                    'data' => $p,
                    'rank' => null,
                ];
            }

            // Classement des joueurs
            $orders = [];
            $rank = [];

            /** @var Player $p */
            foreach ($g->getPlayers() as $p) {
                $id = $p->getId();
                $orders[$id] = $p->getScore();
                $rank[$id] = $id;
            }
            array_multisort($orders, SORT_DESC, $rank);

            foreach ($rank as $num => $id) {
                $games[$g->getId()]['players'][$id]['rank'] = $num + 1;
            }
        }

        return $games;
    }

    public function addGame(Game $game = null, array $users = [], bool $isVirtualDices = false): Game
    {
        if (null === $game) {
            $game = (new Game())
                ->setToken(Text::random())
                ->setIsVirtualDices($isVirtualDices);
        }

        $this->em->persist($game);

        // Ajout des players
        $game = $this->addPlayers($game, $users);

        // Ajout des défis à la partie
        $game = $this->addGoals($game);

        // Ajout des défis aux users
        $this->addStrokes($game);

        $this->em->flush();

        return $game;
    }

    private function addPlayers(Game $game, array $users): Game
    {
        if (!count($users)) {
            $users = $this->userService->getRandomUsers();
        }

        $i = 0;
        foreach ($users as $user) {
            $player = (new Player())
                ->setUser($user)
                ->setToken(Text::random())
                ->setGame($game)
                ->setOrder($i);

            $game->addPlayer($player);
            ++$i;
        }

        return $game;
    }

    /**
     * Ajout des challenges à la partie.
     */
    private function addGoals(Game $game): Game
    {
        $steps = $this->stepService->getStepsByOrder();
        $colorChallenge = $this->em->getRepository(Challenge::class)->findOneBy(['step' => null]);

        foreach ($steps as $step) {
            // Liste des challenges liés à la step
            $challenges = $step->getChallenges()->toArray();
            $nbChallenges = $step->getNbChallenge();
            $nbDid = 0;

            // Ajout aléatoire de challenges
            while ($nbDid < $nbChallenges) {
                $rand = rand(0, count($challenges) - 1);

                /** @var Challenge $challenge */
                $challenge = $challenges[$rand];

                $game = $this->addGoal($game, $challenge, $nbDid);

                // Suppression du challenge pour ne pas le reprendre
                array_splice($challenges, $rand, 1);

                ++$nbDid;
            }

            // Ajout du ou des challendes "colorYams"
            for ($i = 0; $i < $step->getNbColorYams(); ++$i) {
                $game = $this->addGoal($game, $colorChallenge, $nbDid, $step);
                ++$nbDid;
            }
        }

        return $game;
    }

    /**
     * Ajout d'un challenge à la partie.
     */
    private function addGoal(Game $game, Challenge $challenge, int $order, Step $step = null): Game
    {
        $step = $step ?: $challenge->getStep();

        $gameChallenge = (new Goal())
            ->setGame($game)
            ->setStep($step)
            ->setChallenge($challenge)
            ->setOrder($order)
            ->setToken(Text::random());

        $game->addGoal($gameChallenge);

        return $game;
    }

    private function addStrokes(Game $game): void
    {
        /** @var Player $player */
        foreach ($game->getPlayers() as $player) {
            /** @var Goal $goal */
            foreach ($game->getGoals() as $goal) {
                $stroke = (new Stroke())
                    ->setPlayer($player)
                    ->setGoal($goal)
                    ->setToken(Text::random());

                $player->addStroke($stroke);
            }
        }
    }

    public function delete(string $token = null): bool
    {
        $games = [];

        if ($token) {
            $games[] = $this->em->getRepository(Game::class)
                ->findOneBy(['token' => $token]);
        } else {
            $games = $this->em->getRepository(Game::class)->findAll();
        }

        if (!count($games)) {
            return false;
        }

        /** @var Game $game */
        foreach ($games as $game) {
            $this->em->remove($game);
        }

        $this->em->flush();

        return true;
    }

    public function deleteScore(string $token = null): bool
    {
        if (!$token) {
            return false;
        }

        /** @var Stroke|null $stroke */
        $stroke = $this->em->getRepository(Stroke::class)->findOneBy(['token' => $token]);

        if (!$stroke) {
            return false;
        }

        $strokes = $this->em->getRepository(Stroke::class)->findBy(['goal' => $stroke->getGoal()]);

        /** @var Stroke $otherStroke */
        foreach ($strokes as $otherStroke) {
            if ($otherStroke !== $stroke) {
                $otherStroke->setBonus(null);
            }
        }

        $stroke
            ->setScore(null)
            ->setBonus(null);

        $stroke->getPlayer()->getGame()->setIsOver(false);

        $this->em->flush();
        $this->playService->checkBonus($stroke->getPlayer()->getGame());

        return true;
    }

    public function isReloadPossible(Game $game): bool
    {
        /** @var StrokeRepository $repo */
        $repo = $this->em->getRepository(Stroke::class);

        return !$repo->isGameHasStroke($game);
    }

    public function reload(Game $game): Game
    {
        $users = [];
        $goalsId = [];

        /** @var Goal $goal */
        foreach ($game->getGoals() as $goal) {
            $goalsId[] = $goal->getId();
        }

        $strokes = $this->em->getRepository(Stroke::class)->findBy([
            'goal' => $goalsId,
        ]);

        /** @var Stroke $stroke */
        foreach ($strokes as $stroke) {
            $this->em->remove($stroke);
        }

        /** @var Goal $goal */
        foreach ($game->getGoals() as $goal) {
            $game->removeGoal($goal);
            $this->em->remove($goal);
        }

        /** @var Player $player */
        foreach ($game->getPlayers() as $player) {
            $users[] = $player->getUser();
            $game->removePlayer($player);
        }

        $this->em->flush();

        return $this->addGame($game, $users);
    }

    public function isRevengePossible(Game $game): bool
    {
        $players = $game->getPlayers();
        $nbPlayers = count($players);
        $nbStrokes = count($players->first()->getStrokes()) * $nbPlayers;
        /** @var StrokeRepository $repo */
        $repo = $this->em->getRepository(Stroke::class);
        $strokesPlayed = count($repo->getStrokesPlayed($game));

        return $nbStrokes === $strokesPlayed;
    }

    public function revenge(Game $game): Game
    {
        $oldUsers = [];
        foreach ($game->getPlayers() as $player) {
            $oldUsers[] = $player->getUser();
        }

        // Décalage d'un rang des joueurs
        $users[] = $oldUsers[\array_key_last($oldUsers)];
        for ($i = 0; $i < count($oldUsers) - 1; ++$i) {
            $users[] = $oldUsers[$i];
        }

        return $this->addGame(null, $users, $game->getIsVirtualDices());
    }
}
