<?php

namespace App\Service;

use App\Entity\Step;
use Doctrine\ORM\EntityManagerInterface;

class StepService
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return Step[]
     */
    public function getStepsByOrder(): array
    {
        return $this->em->getRepository(Step::class)
            ->findBy([], ['order' => 'ASC']);
    }
}
