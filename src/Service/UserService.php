<?php

namespace App\Service;

use App\Controller\GameController;
use App\Controller\UserController;
use App\Entity\Player;
use App\Entity\User;
use App\Helpers\Tab;
use App\Repository\PlayerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class UserService
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Retourne la liste des users.
     *
     * @see UserController::index()
     *
     * @return User[]
     */
    public function getUsers(): array
    {
        return $this->em->getRepository(User::class)
            ->findBy([], ['name' => 'ASC']);
    }

    /**
     * Retourne un nombre aléatoire de users.
     *
     * @see GameService::addPlayers()
     *
     * @return User[]
     */
    public function getRandomUsers(): array
    {
        $usersIn = [];
        $users = $this->getUsers();
        $max = \rand(2, \count($users));

        for ($i = 1; $i <= $max; ++$i) {
            // User aléatoire
            $randNum = \rand(0, \count($users) - 1);
            $usersIn[] = $users[$randNum];

            // Suppression du user pour ne pas le reprendre
            \array_splice($users, $randNum, 1);
        }

        return $usersIn;
    }

    /**
     * Retourne la liste des users sélectionnables et des players sélectionnés.
     *
     * @see GameController::add()
     */
    public function getUsersPlayers(Request $request): array
    {
        // Liste de tous les users
        $usersOut = $this->getUsers();

        // Tokens des users sélectionnés
        $tokens = $request->get('tokens', []);

        // Token d'un user tout juste sélectionné
        $tokenAdd = $request->get('tokenAdd');

        // Jouer avec des dés virtuels
        $isVirtualDices = 1 === (int) $request->get('isVirtualDices');

        // Token d'un player à supprimer
        $tokenDelete = $request->get('tokenDelete');

        // Réordonnage du tableau des users par leur token
        $usersOut = Tab::reorderBy($usersOut, 'token');

        // Tableau dans lequel on va placer les users choisis (qui deviennent players)
        $usersIn = [];

        // Liste des players déjà présents
        if (\count($tokens)) {
            foreach ($tokens as $token) {
                $usersIn[$token] = $usersOut[$token];
            }
        }

        // Ajout d'un player
        if ($tokenAdd && isset($usersOut[$tokenAdd])) {
            $usersIn[$tokenAdd] = $usersOut[$tokenAdd];
        }

        // Suppression d'un player
        if ($tokenDelete && isset($usersIn[$tokenDelete])) {
            $usersOut[$tokenDelete] = $usersIn[$tokenDelete];
            unset($usersIn[$tokenDelete]);
        }

        return [$usersOut, $usersIn, $isVirtualDices];
    }

    public function getUsersByRanking(): array
    {
        $tab = [];
        $orders = [];
        $names = [];

        foreach ($this->getUsers() as $user) {
            $players = $this->em->getRepository(Player::class)->findBy([
                'user' => $user,
            ]);
            $nbGames = 0;
            $score = 0;

            /** @var Player $player */
            foreach ($players as $player) {
                if ($player->getGame()->getIsOver()) {
                    $score += $player->getScore();
                    ++$nbGames;
                }
            }

            if (!$nbGames) {
                continue;
            }

            $id = $user->getId();
            $name = $user->getName();
            $average = $score / $nbGames;

            $tab[$id] = [
                'name' => $name,
                'image' => $user->getImage(),
                'nbGames' => $nbGames,
                'score' => $score,
                'average' => $average,
            ];

            $orders[$id] = $average;
            $names[$id] = $name;
        }

        \array_multisort($orders, SORT_DESC, $names, SORT_ASC, $tab);

        return $tab;
    }

    public function getUsersByBestScore(): array
    {
        $tab = [];
        $orders = [];
        $names = [];

        /** @var PlayerRepository $repo */
        $repo = $this->em->getRepository(Player::class);
        $players = $repo->getBestScores();

        /** @var Player $player */
        foreach ($players as $player) {
            $user = $player->getUser();
            $id = $user->getId();
            $name = $user->getName();
            $score = $player->getScore();

            if (!isset($tab[$id])) {
                $tab[$id] = [
                    'name' => $name,
                    'image' => $user->getImage(),
                    'data' => $player,
                    'best' => null,
                    'worse' => null,
                ];
            }

            if (!$tab[$id]['best'] || $tab[$id]['best'] < $score) {
                $tab[$id]['best'] = $score;
            }

            if (!$tab[$id]['worse'] || ($score >= 6 && $tab[$id]['worse'] > $score)) {
                $tab[$id]['worse'] = $score;
            }

            $orders[$id] = $tab[$id]['best'];
            $names[$id] = $name;
        }

        \array_multisort($orders, SORT_DESC, $names, SORT_ASC, $tab);

        return $tab;
    }
}
