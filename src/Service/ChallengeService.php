<?php

namespace App\Service;

use App\Entity\Challenge;
use App\Entity\Game;
use App\Entity\Step;
use App\Helpers\Text;
use Doctrine\ORM\EntityManagerInterface;

class ChallengeService
{
    private EntityManagerInterface $em;

    private GameService $gameService;

    private array $challenges1 = [];

    private array $challenges2 = [];

    private array $challenges3 = [];

    /** @var string[] */
    private array $colors = ['bleu', 'orange', 'jaune', 'rose', 'gris', 'vert'];

    private int $width = 16;

    public function __construct(EntityManagerInterface $em, GameService $gs)
    {
        $this->em = $em;
        $this->gameService = $gs;
    }

    public function init(bool $isDelete = false): array
    {
        $this->challenges1 = $this->getChallenge1();
        $this->challenges2 = $this->getChallenge2();
        $this->challenges3 = $this->getChallenge3();

        if ($isDelete) {
            $this->save();
        }

        return [
            $this->challenges1,
            $this->challenges2,
            $this->challenges3,
        ];
    }

    public function getChallenge1(): array
    {
        $tab = [];

        foreach ($this->colors as $color) {
            $tab[] = '<span class="mr5">1 x</span><img src="/img/color/' . $color . '.png" class="">';
        }

        foreach ($this->colors as $color) {
            $tab[] = '<span class="mr5">2 x</span><img src="/img/color/' . $color . '.png" class="">';
        }

        foreach ($this->colors as $color) {
            $tab[] = '<img src="/img/color/no' . $color . '.png" class="">';
        }

        foreach ($this->colors as $color) {
            foreach ($this->colors as $color2) {
                if ($color !== $color2) {
                    $challenge = '<img src="/img/color/no' . $color . '.png" class=" mr10"> ';
                    $challenge .= '<img src="/img/color/no' . $color2 . '.png" class="">';
                    $tab[] = $challenge;
                }
            }
        }

        foreach ($this->colors as $k => $color) {
            foreach ($this->colors as $color2) {
                if ($color !== $color2) {
                    $challenge = '1 x <img src="/img/color/' . $color . '.png" class="">';
                    $challenge .= '<span class="ml5 mr5">&</span>';
                    $challenge .= '<img src="/img/color/no' . $color2 . '.png" class="">';
                    $tab[] = $challenge;
                }
            }
        }

        return $tab;
    }

    public function getChallenge2(): array
    {
        $tab[] = '<span class="mr5">3 x</span><img src="/img/color/allcolor.png" class="">';
        $tab[] = '<span class="mr5">Aucun 3 x</span><img src="/img/color/allcolor.png" class="">';
        $tab[] = '<span class="mr5">4 x</span><img src="/img/color/allcolor.png" class="">';

        $tab[] = '<span class="mr5">2 x</span>
<img src="/img/color/random1.png" class=" mr10">2 x
<img src="/img/color/random3.png" class="">';
        $tab[] = '<img src="/img/color/random1.png" class=" mr10">
<img src="/img/color/random3.png" class=" mr10">
<img src="/img/color/random2.png" class="">';

        return $tab;
    }

    public function getChallenge3(): array
    {
        $tab = [];
        $width = $this->width + 1;
        $egal = '<span class="ml10 mr10 fs' . $width . '">=</span>';
        $diff = '<span class="ml10 mr10 fs' . $width . '">≠</span>';
        $sup = '<span class="ml10 mr10">></span>';
        $inf = '<span class="ml10 mr10"><</span>';

        foreach ($this->colors as $color) {
            foreach ($this->colors as $color2) {
                if ($color !== $color2) {
                    $challenge = '<img src="/img/color/' . $color . '.png" class="">';
                    $challenge .= $egal;
                    $challenge .= '<img src="/img/color/' . $color2 . '.png" class="">';
                    $tab[] = $challenge;
                }
            }
        }

        foreach ($this->colors as $color) {
            foreach ($this->colors as $color2) {
                if ($color !== $color2) {
                    $challenge = '<img src="/img/color/' . $color . '.png" class="">';
                    $challenge .= $diff;
                    $challenge .= '<img src="/img/color/' . $color2 . '.png" class="">';
                    $tab[] = $challenge;
                }
            }
        }

        foreach ($this->colors as $color) {
            foreach ($this->colors as $color2) {
                if ($color !== $color2) {
                    $challenge = '<img src="/img/color/' . $color . '.png" class="">';
                    $challenge .= $sup;
                    $challenge .= '<img src="/img/color/' . $color2 . '.png" class="">';
                    $tab[] = $challenge;
                }
            }
        }

        //        foreach ($this->colors as $color) {
        //            foreach ($this->colors as $color2) {
        //                if ($color !== $color2) {
        //                    $challenge = '<img src="/img/color/' . $color . '.png" class="">';
        //                    $challenge .= $inf;
        //                    $challenge .= '<img src="/img/color/' . $color2 . '.png" class="">';
        //                    $tab[]     = $challenge;
        //                }
        //            }
        //        }

        foreach ($this->colors as $color1) {
            foreach ($this->colors as $color2) {
                foreach ($this->colors as $color3) {
                    if ($color1 === $color2 || $color1 === $color3 || $color2 === $color3) {
                        continue;
                    }
                    $challenge = '<img src="/img/color/' . $color1 . '.png" class="">';
                    $challenge .= $diff;
                    $challenge .= '<img src="/img/color/' . $color2 . '.png" class="">';
                    $challenge .= $diff;
                    $challenge .= '<img src="/img/color/' . $color3 . '.png" class="">';
                    $tab[] = $challenge;
                }
            }
        }

        return $tab;
    }

    private function save(): void
    {
        $games = $this->em->getRepository(Game::class)->findAll();

        /** @var Game $game */
        foreach ($games as $game) {
            $this->gameService->delete($game->getToken());
        }

        $challenges = $this->em->getRepository(Challenge::class)->findAll();

        /** @var Challenge $challenge */
        foreach ($challenges as $challenge) {
            if (null === $challenge->getStep()) {
                continue;
            }
            if (\in_array($challenge->getStep()->getId(), [1, 2, 3])) {
                $this->em->remove($challenge);
            }
        }

        $step1 = $this->em->getRepository(Step::class)->find(1);
        $step2 = $this->em->getRepository(Step::class)->find(2);
        $step3 = $this->em->getRepository(Step::class)->find(3);

        foreach ($this->challenges1 as $name) {
            $challenge = (new Challenge())
                ->setName($name)
                ->setToken(Text::random())
                ->setStep($step1)
                ->setImage('');
            $this->em->persist($challenge);
        }

        foreach ($this->challenges2 as $name) {
            $challenge = (new Challenge())
                ->setName($name)
                ->setToken(Text::random())
                ->setStep($step2)
                ->setImage('');
            $this->em->persist($challenge);
        }

        foreach ($this->challenges3 as $name) {
            $challenge = (new Challenge())
                ->setName($name)
                ->setToken(Text::random())
                ->setStep($step3)
                ->setImage('');
            $this->em->persist($challenge);
        }

        $this->em->flush();
    }
}
