<?php

namespace App\Service;

use App\Entity\Game;
use App\Entity\Player;
use App\Entity\Stroke;
use App\Repository\StrokeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;

class PlayService
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly StepService $stepService,
        private readonly UserService $userService
    ) {
    }

    public function checkBonus(Game $game): array|string
    {
        /** @var StrokeRepository $repo */
        $repo = $this->em->getRepository(Stroke::class);
        $strokes = $repo->getWithoutBonus($game);
        $nbStrokes = \count($strokes);
        $nbPlayers = \count($game->getPlayers());
        $ratio = $nbStrokes / $nbPlayers;
        $logs = [];

        // 3 goals sans bonus et 3 joueurs
        // ou aucun bonus calculé et on a 10 bonus NULL et N joueurs
        $isToCheck = $ratio === (int) $ratio || $nbStrokes > $nbPlayers;

        if (!$isToCheck) {
            return 'NO BONUS TO CHECK';
        }

        $tab = [];
        $tabStrokes = [];

        /** @var Stroke $stroke */
        foreach ($strokes as $stroke) {
            $gId = $stroke->getGoal()->getId();
            $sId = $stroke->getId();

            $tabStrokes[$sId] = $stroke;

            if (!isset($tab[$gId])) {
                $tab[$gId] = [
                    'strokes' => [],
                    'points' => [],
                ];
            }

            $tab[$gId]['strokes'][$sId] = $stroke;
        }

        foreach ($tab as $gId => $goal) {
            // Cas où aucun bonus n'a été calculé
            // mais on est sur un goal non terminé par un joueur
            if (\count($goal['strokes']) !== $nbPlayers) {
                continue;
            }

            $bonus = $nbPlayers;
            $scoreOld = null;

            $podiums = [];
            $strokes = [];

            /**
             * @var int    $sId
             * @var Stroke $stroke
             */
            foreach ($goal['strokes'] as $sId => $stroke) {
                $score = $stroke->getScore();
                $strokes[$sId] = $score;

                if (null !== $scoreOld) {
                    if ($score !== $scoreOld) {
                        --$bonus;
                    }
                }

                $podiums[$sId] = $bonus;
                $scoreOld = $score;
            }

            foreach ($podiums as $sId => $bonus) {
                $logs[] = "BONUS DE $sId : $bonus";

                $tabStrokes[$sId]->setBonus($bonus);
            }
        }

        $this->em->flush();
        $this->em->refresh($game);

        return [
            'strokes' => $strokes,
            'nbPlayers' => $nbPlayers,
            'isToCheck' => true,
            'tab' => $tab,
            'logs' => $logs,
        ];
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getLastColorYams(Stroke $stroke): ?Stroke
    {
        /** @var StrokeRepository $repo */
        $repo = $this->em->getRepository(Stroke::class);

        return $repo->getLastColorYams($stroke);
    }

    public function checkColorYams(Stroke $stroke, int $score): int
    {
        // Le coup n'est pas un Color Yams
        if ($stroke->getGoal()->getChallenge()->getStep()) {
            return $score;
        }

        $lastStroke = $this->getLastColorYams($stroke);

        // Pas d'ancien Color Yams
        if (null === $lastStroke) {
            return $score;
        }

        return $score > $lastStroke->getScore() ? $score : 0;
    }

    /**
     * @return void
     */
    public function countScores(Game $game)
    {
        $players = [];

        /** @var Player $player */
        foreach ($game->getPlayers() as $player) {
            $players[$player->getId()] = $player;
        }

        $scores = [];
        $positions = [];
        /** @var StrokeRepository $repo */
        $repo = $this->em->getRepository(Stroke::class);
        $strokes = $repo->getStrokesByGame($game);

        foreach ($strokes as $stroke) {
            /** @var Player $player */
            $player = $stroke->getPlayer();
            $pId = $player->getId();

            if (!isset($scores[$pId])) {
                $scores[$pId] = 0;
            }

            $scores[$pId] += $stroke->getScore() + $stroke->getBonus();
        }

        foreach ($players as $pId => $player) {
            $player->setScore($scores[$pId]);
            $positions[$scores[$pId]] = $pId;
        }

        $i = 1;
        foreach ($positions as $k => $pId) {
            $players[$pId]->setPosition($i);
            ++$i;
        }

        $this->em->flush();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function play(Game $game, Request $request): array
    {
        /** @var StrokeRepository $repo */
        $repo = $this->em->getRepository(Stroke::class);
        $stroke = $repo->getNextStroke($game);
        $score = $request->get('score');

        $this->countScores($game);

        // Partie finie
        if (null === $stroke) {
            $this->countScores($game);
            $game->setIsOver(true);
            $this->em->flush();

            return ['stroke' => $stroke, 'score' => $score];
        }

        // Saisie d'un score
        if (null !== $score) {
            $score = $this->checkColorYams($stroke, $score);
            $stroke->setScore($score);
            $this->em->flush();
            $this->checkBonus($game);

            return ['stroke' => $stroke, 'score' => $score];
        }

        $player = $stroke->getPlayer();
        $goal = $stroke->getGoal();
        $step = $goal->getStep();
        $challenge = $goal->getChallenge();
        $game = $player->getGame();
        $user = $player->getUser();
        $lastColorYams = $this->getLastColorYams($stroke);

        return [
            'stroke' => $stroke,
            'player' => $player,
            'game' => $game,
            'user' => $user,
            'challenge' => $challenge,
            'step' => $step,
            'goal' => $goal,
            'lastColorYams' => $lastColorYams,
            'score' => $score,
        ];
    }
}
