<?php

namespace App\Service;

use App\Entity\Game;
use App\Entity\Goal;
use App\Entity\Step;
use App\Entity\Stroke;
use App\Repository\StrokeRepository;
use Doctrine\ORM\EntityManagerInterface;

class GridService
{
    private EntityManagerInterface $em;

    private StepService $stepService;

    public function __construct(EntityManagerInterface $em, StepService $stepService)
    {
        $this->em = $em;
        $this->stepService = $stepService;
    }

    public function getGrid(Game $game): array
    {
        $tab = [
            'players' => [],
            'steps' => [],
        ];

        // Joueurs
        foreach ($game->getPlayers() as $player) {
            $playerId = $player->getId();
            $user = $player->getUser();

            $tab['players'][$playerId] = [
                'id' => $playerId,
                'token' => $player->getToken(),
                'name' => $user->getName(),
                'alias' => $user->getAlias(),
                'image' => $user->getImage(),
                'score' => 0, // $player->getScore(),
                'bonus' => 0,
                'rank' => null,
            ];
        }

        // Liste des étapes
        $steps = $this->stepService->getStepsByOrder();

        // Liste des challenges classés par step
        $goals = $this->getGridGoals($game, $tab['players']);

        foreach ($steps as $step) {
            $stepId = $step->getId();

            $tab['steps'][$stepId] = [
                'id' => $stepId,
                'token' => $step->getToken(),
                'color' => $step->getColor(),
                'goals' => $goals[$stepId],
                'players' => $tab['players'],
            ];

            // Boucle sur les goals de la step
            foreach ($goals[$stepId] as $goal) {
                // Boucle sur les players du goal
                foreach ($goal['players'] as $playerId => $player) {
                    $scoreBonus = $player['score'] + $player['bonus'];

                    $tab['steps'][$stepId]['players'][$playerId]['score'] += $scoreBonus;
                    $tab['players'][$playerId]['score'] += $scoreBonus;
                }
            }
        }

        // Classement des joueurs
        $orders = [];
        $rank = [];
        foreach ($tab['players'] as $id => $player) {
            $orders[(int) $id] = $player['score'];
            $rank[(int) $id] = $id;
        }
        \array_multisort($orders, \SORT_DESC, $rank);

        $tab['rank'] = [];
        foreach ($rank as $num => $id) {
            $tab['players'][$id]['rank'] = $num + 1;
        }

        return $tab;
    }

    private function getGridGoals(Game $game, array $players): array
    {
        $tab = [];

        $goals = $this->em->getRepository(Goal::class)
            ->findBy(['game' => $game], ['id' => 'ASC']);

        $strokes = $this->getGridStrokes($goals, $players);

        /** @var Goal $goal */
        foreach ($goals as $goal) {
            $challenge = $goal->getChallenge();
            $stepId = $goal->getStep()->getId();
            $goalId = $goal->getId();
            $isColor = !$challenge->getStep();
            $isYams = $isColor;
            $alpha = $isColor ? 1 : 0.5;
            $alphaLight = $alpha / 3;

            $tab[$stepId][$goalId] = [
                'id' => $goalId,
                'token' => $goal->getToken(),
                'name' => $challenge->getName(),
                'isColor' => $isColor,
                'alpha' => $alpha,
                'alphaLight' => $alphaLight,
                'players' => $players,
                'isColorYams' => $isYams,
            ];

            foreach ($players as $player) {
                $score = $strokes[$stepId][$goalId][$player['id']]['score'];
                $bonus = $strokes[$stepId][$goalId][$player['id']]['bonus'];
                $strokeToken = $strokes[$stepId][$goalId][$player['id']]['strokeToken'];

                $tab[$stepId][$goalId]['players'][$player['id']]['score'] = $score;
                $tab[$stepId][$goalId]['players'][$player['id']]['bonus'] = $bonus;
                $tab[$stepId][$goalId]['players'][$player['id']]['strokeToken'] = $strokeToken;
            }
        }

        return $tab;
    }

    private function getGridStrokes(array $goals, array $players): array
    {
        $tabP = [];
        $tab = [];

        foreach ($players as $player) {
            $tabP[$player['id']] = $player['id'];
        }

        $strokes = $this->em->getRepository(Stroke::class)->findBy([
            'goal' => $goals,
            'player' => $tabP,
        ]);

        /** @var Stroke $stroke */
        foreach ($strokes as $stroke) {
            $goal = $stroke->getGoal();
            $stepId = $goal->getStep()->getId();
            $goalId = $goal->getId();
            $playerId = $stroke->getPlayer()->getId();

            $tab[$stepId][$goalId][$playerId] = [
                'score' => $stroke->getScore(),
                'bonus' => $stroke->getBonus(),
                'strokeId' => $stroke->getId(),
                'strokeToken' => $stroke->getToken(),
            ];
        }

        return $tab;
    }

    /**
     * @return array|array[]
     */
    public function getGrid2(Game $game): array
    {
        $tab = ['steps' => [], 'scores' => [], 'players' => []];
        $players = [];

        /** @var StrokeRepository $repo */
        $repo = $this->em->getRepository(Stroke::class);
        $strokes = $repo->getGrid2($game);

        foreach ($strokes as $stroke) {
            $spId = $stroke['stepId'];
            $gId = $stroke['goalId'];
            $skId = $stroke['strokeId'];
            $pId = $stroke['playerId'];
            $uId = $stroke['userId'];

            // Création du player
            $players[$pId] = $this->addPlayer($tab['players'], $pId, $stroke);

            // Création de la step
            $tab['steps'][$spId] = $this->addStep($tab['steps'], $spId, $stroke);

            // Création du goal
            $tab['steps'][$spId]['goals'][$gId] = $this->addGoal($tab['steps'][$spId]['goals'], $gId, $stroke);

            // Création du stroke
            $tab['steps'][$spId]['goals'][$gId]['strokes'][$skId] = $stroke;
        }

        $tab['players'] = $players;

        return $tab;
    }

    private function addStep(array $tab, int $id, array $stroke): array
    {
        if (!isset($tab[$id])) {
            $tab[$id] = [
                'id' => $id,
                'color' => $stroke['color'],
                'scores' => [],
                'goals' => [],
            ];
        }

        // Cumul des scores du player
        $tab[$id] = $this->addScore($tab[$id], $stroke);

        return $tab[$id];
    }

    private function addGoal(array $tab, int $id, array $stroke): array
    {
        if (!isset($tab[$id])) {
            $tab[$id] = [
                'id' => $id,
                'name' => $stroke['challengeName'],
                'scores' => [],
                'strokes' => [],
            ];
        }

        // Cumul des scores du player
        $tab[$id] = $this->addScore($tab[$id], $stroke);

        return $tab[$id];
    }

    /**
     * @return array|mixed
     */
    private function addPlayer(array $tab, int $id, array $stroke)
    {
        if (isset($tab[$id])) {
            return $tab[$id];
        }

        return [
            'id' => $stroke['playerId'],
            'userid' => $stroke['userId'],
            'name' => $stroke['userName'],
            'image' => $stroke['userImage'],
            'score' => $stroke['strokeScore'],
            'bonus' => $stroke['strokeBonus'],
        ];
    }

    /**
     * Cumul du score dans les steps, goals, ...
     */
    private function addScore(array $tab, array $stroke): array
    {
        $pId = $stroke['playerId'];
        $total = $stroke['strokeScore'] + $stroke['strokeBonus'];

        if (!isset($tab['scores'][$pId])) {
            $tab['scores'][$pId] = 0;
        }

        $tab['scores'][$pId] += $total;

        return $tab;
    }
}
