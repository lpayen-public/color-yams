<?php

namespace App\Repository;

use App\Entity\Challenge;
use App\Entity\Game;
use App\Entity\Goal;
use App\Entity\Player;
use App\Entity\Step;
use App\Entity\Stroke;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Stroke|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stroke|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stroke[]    findAll()
 * @method Stroke[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StrokeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stroke::class);
    }

    public function add(Stroke $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove(Stroke $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getNextStroke(Game $game): ?Stroke
    {
        $qb = $this->createQueryBuilder('stroke')
            ->join(Player::class, 'player', Join::WITH, 'stroke.player = player')
            ->join(Goal::class, 'goal', Join::WITH, 'stroke.goal = goal')
            ->join(Step::class, 'step', Join::WITH, 'goal.step = step')
            ->join(Game::class, 'game', Join::WITH, 'goal.game = game')
            ->andWhere('stroke.score IS NULL')
            ->andWhere('game = :game')
            ->setParameter('game', $game)
            ->setMaxResults(1)
            ->addOrderBy('step.order', 'ASC')
            ->addOrderBy('goal.order', 'ASC')
            ->addOrderBy('player.order', 'ASC')
            ->addOrderBy('stroke.id', 'ASC');

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return float|int|mixed|string
     */
    public function getGrid2(Game $game)
    {
        $qb = $this->createQueryBuilder('stroke');
        $qb->select('
            stroke.id as strokeId, stroke.score as strokeScore, stroke.bonus as strokeBonus,
            goal.id as goalId, challenge.id as challengeId, challenge.name as challengeName,
            step.id as stepId, step.color,
            player.id as playerId, player.order as playerOrder,
            user.id as userId, user.name as userName, user.image as userImage
        ')
            ->join(Player::class, 'player', Join::WITH, 'stroke.player = player')
            ->join(User::class, 'user', Join::WITH, 'player.user = user')
            ->join(Goal::class, 'goal', Join::WITH, 'stroke.goal = goal')
            ->join(Challenge::class, 'challenge', Join::WITH, 'goal.challenge = challenge')
            ->join(Game::class, 'game', Join::WITH, 'goal.game = game')
            ->join(Step::class, 'step', Join::WITH, 'goal.step = step')
            ->andWhere('game = :game')
            ->setParameter('game', $game)
            ->addOrderBy('step.order', 'ASC')
            ->addOrderBy('goal.order', 'ASC')
            ->addOrderBy('player.order', 'ASC')
            ->addOrderBy('stroke.id', 'ASC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @return float|int|mixed|string
     */
    public function getWithoutBonus(Game $game)
    {
        $qb = $this->createQueryBuilder('stroke')
            ->join(Player::class, 'player', Join::WITH, 'stroke.player = player')
            ->join(Game::class, 'game', Join::WITH, 'player.game = game')
            ->join(Goal::class, 'goal', Join::WITH, 'stroke.goal = goal')
            ->join(Step::class, 'step', Join::WITH, 'goal.step = step')
            ->andWhere('game = :game')
            ->setParameter('game', $game)
            ->andWhere('stroke.score IS NOT NULL')
            ->andWhere('stroke.bonus IS NULL')
            ->addOrderBy('step.order', 'ASC')
            ->addOrderBy('goal.order', 'ASC')
            ->addOrderBy('stroke.score', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getLastColorYams(Stroke $stroke): ?Stroke
    {
        $game = $stroke->getGoal()->getGame();
        $player = $stroke->getPlayer();

        $qb = $this->createQueryBuilder('stroke')
            ->join(Player::class, 'player', Join::WITH, 'stroke.player = player')
            ->join(Game::class, 'game', Join::WITH, 'player.game = game')
            ->join(Goal::class, 'goal', Join::WITH, 'stroke.goal = goal')
            ->join(Challenge::class, 'challenge', Join::WITH, 'goal.challenge = challenge')
            ->andWhere('game = :game')
            ->andWhere('player = :player')
            ->andWhere('challenge.step IS NULL')
            ->andWhere('stroke.score IS NOT NULL')
            ->setParameters([
                'game' => $game,
                'player' => $player,
            ])
            ->setMaxResults(1)
            ->addOrderBy('stroke.score', 'DESC');

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return Stroke[]
     */
    public function getStrokesByGame(Game $game): array
    {
        $qb = $this->createQueryBuilder('stroke')
            ->join(Player::class, 'player', Join::WITH, 'stroke.player = player')
            ->join(Game::class, 'game', Join::WITH, 'player.game = game')
            ->andWhere('game = :game')
            ->setParameter('game', $game);

        return $qb->getQuery()->getResult();
    }

    public function getStrokesPlayed(Game $game): array
    {
        $qb = $this->createQueryBuilder('stroke')
            ->select('stroke.id')
            ->join(Player::class, 'player', Join::WITH, 'stroke.player = player')
            ->join(Game::class, 'game', Join::WITH, 'player.game = game')
            ->andWhere('game = :game')
            ->andWhere('stroke.score IS NOT NULL')
            ->setParameter('game', $game);

        return $qb->getQuery()->getResult();
    }

    public function isGameHasStroke(Game $game): bool
    {
        return (bool) count($this->getStrokesPlayed($game));
    }
}
